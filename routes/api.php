<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group(['prefix' => 'v1'], function () {

    Route::group(['prefix' => 'public'], function () {

        // Healthz Status
        Route::get('healthz', 'PublicController@healthz');

        // Auth Route
        Route::group(['prefix' => 'auth'], function () {
            Route::post('signup', 'API\v1\Auth\AuthController@register');
            Route::post('login', 'API\v1\Auth\AuthController@login');
            Route::post('verify-email', 'API\v1\Auth\AuthController@verifyEmail');
            Route::post('verify-phone', 'API\v1\Auth\AuthController@verifyPhone');
            Route::post('resend-email-verification', 'API\v1\Auth\AuthController@resendEmailVerification');
            Route::post('resend-phone-verification', 'API\v1\Auth\AuthController@resendPhoneVerification');
            Route::post('forgot-password', 'API\v1\Auth\AuthController@forgotPassword');
            Route::post('recover-password', 'API\v1\Auth\AuthController@recoverPassword');
            Route::post('send-email-token', 'API\v1\Auth\AuthController@sendEmailToken');
            Route::post('validate-email-token', 'API\v1\Auth\AuthController@validateEmailToken');
        });

        // Banks
        Route::group(['prefix' => 'banks'], function () {
            Route::get('/', 'API\v1\Bank\BankController@index');
            Route::get('{bank_id}', 'API\v1\Bank\BankController@get');
            Route::post('resolve-account', 'API\v1\Bank\BankController@resolveAccount');
        });

    });

    Route::group(['middleware' => 'auth:api', 'prefix' => 'user'], function () {
        Route::get('/', 'API\v1\User\UserController@getUser');
    });

    Route::group(['middleware' => 'auth:api'], function () {

        Route::group(['prefix' => 'wallet'], function () {
            Route::post('create-wallet', 'API\v1\Wallet\WalletController@createWallet');
            Route::post('credit-wallet', 'API\v1\Wallet\WalletController@fundWallet');
            Route::post('bank-transfer', 'API\v1\Wallet\WalletController@bankTransfer');
            Route::post('transfer-to-user', 'API\v1\Wallet\WalletController@userTransfer');
        });

        // Accounts Route
        Route::group(['prefix' => 'account'], function () {
            Route::get('generate-account-number', 'API\v1\Account\AccountController@generateAccountNumber');
            Route::post('monnify/callback', 'API\v1\Account\AccountController@webhook');
        });

        // Savings Route
        Route::group(['prefix' => 'saving'], function () {
            Route::post('one-time/create', 'API\v1\Saving\SavingController@oneTime');
            Route::post('stop', 'API\v1\Saving\SavingController@oneTime');
        });
    });
});
