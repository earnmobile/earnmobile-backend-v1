<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Saving extends Model
{
    protected $guarded = ['id'];


    /**
     * Get the wallet that owns the Saving
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wallet(): BelongsTo
    {
        return $this->belongsTo(Wallet::class);
    }

    /**
     * Get all of the interests for the Saving
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function interests(): HasMany
    {
        return $this->hasMany(Interest::class);
    }

    /**
     * Get the user that owns the Saving
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }


    /**
     * Get the type that owns the Saving
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(SavingType::class, 'type');
    }
    
}
