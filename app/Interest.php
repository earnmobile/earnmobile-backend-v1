<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
    protected $guarded = ['id'];


    /**
     * Get the saving that owns the Interest
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function saving(): BelongsTo
    {
        return $this->belongsTo(Saving::class);
    }

    
}
