<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ValidToken extends Model
{
    protected $table = 'valid_tokens';
    protected $guarded = ['id'];
}
