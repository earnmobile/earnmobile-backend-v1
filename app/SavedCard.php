<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavedCard extends Model
{
    protected $guarded = ['id'];

    /**
     * Get the user that owns the SavedCard
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
