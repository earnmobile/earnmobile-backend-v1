<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    protected $guarded = ['id'];


    /**
     * Get the transaction that owns the Transfer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transaction(): BelongsTo
    {
        return $this->belongsTo(Transaction::class);
    }


    /**
     * Get the wallet that owns the Transfer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wallet(): BelongsTo
    {
        return $this->belongsTo(Wallet::class);
    }
    

    /**
     * Get the benefactor that owns the Transfer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function benefactor()
    {
        if ($this->to_user == true) {
            return $this->belongsTo(User::class, 'benefactor_id');
        }else{
            return $this->belongsTo(Benefactor::class, 'benefactor_id');
        }
    }

    
}
