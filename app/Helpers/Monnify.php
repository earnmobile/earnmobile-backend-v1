<?php


namespace App\Helpers;


use GuzzleHttp\Client;

class Monnify
{
    public static function authenticate()
    {
        $client = new Client();
        $url = config('monnify.base_url');
        $token = base64_encode(config('monnify.api_key') . ':' . config('monnify.secret_key'));
        $headers = [
            'Authorization' => 'Basic ' . $token
        ];

        $response = $client->request('POST', $url . 'api/v1/auth/login', [
            'headers' => $headers,
        ]);

        return json_decode($response->getBody());
    }
}
