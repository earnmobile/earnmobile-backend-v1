<?php

use Illuminate\Support\Facades\Http;

if (!function_exists('generateToken')) {
    function generateToken(): int
    {
        return mt_rand(100000, 999999);
    }
}

if (!function_exists('generatePasswordResetCode')) {
    function generatePasswordResetCode(): string
    {
        $partOne = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 3);
        $partTwo = substr(str_shuffle("0123456789"), 0, 4);
        $code = $partOne . $partTwo;
        return str_shuffle($code);
    }
}

if (!function_exists('generateRandomKeys')) {
    function generateRandomKeys(): string
    {
        $partOne = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 3);
        $partTwo = substr(str_shuffle("0123456789"), 0, 8);
        $code = $partTwo . $partOne;
        return str_shuffle($code);
    }
}

if (!function_exists('sendTokenViaSMS')) {
    function sendTokenViaSMS($to, $token)
    {
        $message = 'Your Earnmobile confirmation code is ' . $token . '. It expires in 10 Minutes';
        $curl = curl_init();

        $data = array(
            "to" => $to,
            "from" => config('termii.sender'),
            "sms" => $message,
            "type" => "plain",
            "channel" => "dnd",
            "api_key" => config('termii.api_key'),
        );

        $post_data = json_encode($data);

        curl_setopt_array($curl, array(
            CURLOPT_URL => config('termii.base_url') . "/sms/send",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $post_data,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
}

if (!function_exists('payWithCard')) {
    function payWithCard($encryptedData): array
    {
        $key = config('services.flutter-wave.sk');
        $endpoint = config('services.flutter-wave.url') . 'charges?type=card';
        $response = Http::withHeaders(['Authorization' => 'Bearer ' . $key])->post($endpoint, [
            'client' => $encryptedData,
        ])->json();

        if ($response['status'] == 'success') {
            return [
                'status' => true,
                'data' => $response,
                'err' => null
            ];
        } else {
            return [
                'status' => false,
                'data' => null,
                'err' => $response['message']
            ];
        }

    }
}

if (!function_exists('verifyPayment')) {
    function verifyPayment($transat_id): array
    {
        $key = config('services.flutter-wave.sk');
        $endpoint = config('services.flutter-wave.url') . "transactions/$transat_id/verify";
        $response = Http::withHeaders(['Authorization' => 'Bearer ' . $key])->get($endpoint)->json();
        if ($response['status'] == 'success') {
            return [
                'status' => true,
                'data' => [
                    'status' => $response['data']['status'],
                    'currency' => $response['data']['currency'],
                    'amount' => $response['data']['amount'],
                    'reference' => $response['data']['tx_ref'],
                ],
                'err' => null
            ];
        } else {
            return [
                'status' => false,
                'data' => null,
                'err' => $response['message']
            ];
        }
    }
}


if (!function_exists('validateAccount')) {
    function validateAccount($account_number, $bank): array
    {
        $endpoint = config('services.flutter-wave.url') . 'accounts/resolve';
        $key = config('services.flutter-wave.sk');

        $response = Http::withHeaders(['Authorization' => 'Bearer ' . $key])->post($endpoint, [
            'account_number' => $account_number,
            'account_bank' => $bank
        ])->json();

        if ($response['status'] == 'success') {
            return ['status' => true, 'data' => $response['data']];
        }

        return ['status' => false, 'data' => null, 'message' => $response['message']];
    }
}


if (!function_exists('openSslEncryption')) {
    function openSSLEncryption($data, $key): string
    {
        $encData = openssl_encrypt($data, 'DES-EDE3', $key, OPENSSL_RAW_DATA);
        return base64_encode($encData);
    }
}

if (!function_exists('generateTxCode')) {
    function generateCode(): string
    {
        $time = substr(time(), 6, 4);
        $random = mt_rand(1000, 9999);
        return 'EMTX-' . $time . '-' . $random;
    }
}

if (!function_exists('trx_fee')) {
    function trx_fee($amount)
    {
        $percentage = config('app.fee');
        return ($amount / 100) * $percentage;
    }
}

if (!function_exists('cleanupPhone')) {
    function cleanupPhone($str)
    {
        $prefix = '+';

        if (substr($str, 0, strlen($prefix)) == $prefix) {
            $str = substr($str, strlen($prefix));
        }

        return $str;
    }
}

if (!function_exists('checkEmail')) {
    function checkEmail($credential): bool
    {
        $has_at_symbol = strpos($credential, '@');
        $has_dot = strpos($credential, '.');
        $is_email = $has_at_symbol !== false && $has_dot !== false && $has_at_symbol > $has_dot;
        return $is_email == true;
    }
}


if (!function_exists('trim_phone')) {
    function trim_phone($phone)
    {
        $mobile_no = $phone;
        if (str_contains($mobile_no, '+234') === false) {
            $mobile_no = ltrim($mobile_no, "0");
            $mobile_no = '+234' . $mobile_no;
        }

        return $mobile_no;
    }
}


