<?php

namespace App\Repositories;

use App\Transaction;

class TransactionRepository {

    /**
     * Get all transactions
     */
    public function all()
    {
        return Transaction::orderBy('created_at', 'desc')->get();
    }

    /**
     * Get User's Transactions @param collection $user
     */
    public function userTransactions($user)
    {
        return $user->transactions;
    }


    /**
     * Get transaction by id @param int $transaction_id
     */
    public function get($transaction_id)
    {
        return Transaction::where('id', $transaction_id)->first();
    }


    /**
     * Create a new Transaction @param object $data
     */
    public function create($data)
    {
        $transaction =  Transaction::create([
            'trx_id' => $data['trx_ref'],
            'type' => $data['type'],
            'amount' => $data['amount'],
            'fee' => $data['fee'] ? $data['fee'] : 0,
            'reason' => $data['reason'],
            'status' => $data['status'],
            'previous_balance' => $data['previous_balance'],
            'new_balance' => $data['new_balance'],
        ]);

        return $transaction;
    }

}