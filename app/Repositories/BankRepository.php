<?php

namespace App\Repositories;

use App\Bank;

class BankRepository {

    public function all()
    {
        return Bank::orderBy('name', 'asc')->get()->map->formatOutput();
    }


    public function get($bank_id)
    {
        return Bank::where('id', $bank_id)->first();
    }


    public function resolveAccount($request)
    {
        return validateAccount($request->account_number, $request->bank);
    }

}
