<?php


namespace App\Repositories;


use App\Account;
use App\Profile;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Auth;

class AccountRepository
{
    private $modelInstance;

    public function __construct(Account $user)
    {
        $this->modelInstance = $user;
    }

    /**
     * @throws GuzzleException
     */
    public function generateAccountNumber($monnifyToken)
    {
        // Logged User Info
        $user = Auth::user();
        $userProfile = Profile::whereUserId($user->id)->first();

        // Api Call Begins
        $client = new Client();

        $url = config('monnify.base_url') . 'api/v2/bank-transfer/reserved-accounts';

        $response = $client->post($url, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $monnifyToken,
            ],
            'json' => [
                'accountReference' => 'EMB' . generateRandomKeys(),
                'accountName' => 'Robert Ebafua',
                'currencyCode' => 'NGN',
                'contractCode' => config('monnify.contract_code'),
                'customerEmail' => $user->email,
                'bvn' => '22237540924',
                'customerName' => $userProfile->firstname . ' ' . $userProfile->lastname,
                'getAllAvailableBanks' => false,
                'preferredBanks' => ["035"]
            ]
        ]);

        $data =  json_decode($response->getBody());

        if ($data->requestSuccessful) {
            $this->modelInstance::create([
                'user_id' => $user->id,
                'bank_name' => $data->responseBody->accounts[0]->bankName,
                'bank_code' => $data->responseBody->accounts[0]->bankCode,
                'name' => $data->responseBody->accountName,
                'number' => $data->responseBody->accounts[0]->accountNumber,
                'reference' => $data->responseBody->accountReference,
            ]);
        }

        return $data;

    }
}
