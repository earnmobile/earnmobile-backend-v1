<?php

namespace App\Repositories;

use App\User;
use Illuminate\Support\Facades\Auth;

class UserRepository {
    private $modelInstance;

    public function __construct(User $user)
    {
        $this->modelInstance = $user;
    }

    public function getUser() {
        // Logged User Info
        $user = Auth::user();

        return User::with('profile')->where('id', $user->id)->first();

    }
}
