<?php

namespace App\Repositories;

use App\Blacklist;
use App\Mail\ForgotPasswordEmail;
use App\Mail\SendEmailToken;
use App\Mail\VerifyEmail;
use App\Profile;
use App\Token;
use App\User;
use App\ValidToken;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use JWTAuth;

class AuthRepository
{

    private $modelInstance;

    public function __construct(User $user)
    {
        $this->modelInstance = $user;
    }

    public function registerUser($dataToCreate)
    {
        $emailToken = generateToken();
        $phoneToken = generateToken();
        $dataToCreate['password'] = Hash::make($dataToCreate['password']);

        // Send Code to Email
        $data = [
            'email' => $dataToCreate['email'],
            'name' => $dataToCreate['firstname'] . ' ' . $dataToCreate['lastname'],
            'verification_code' => $emailToken,
        ];

        //Mail::to($dataToCreate['email'])->send(new VerifyEmail($data));

        // // Send Code to Phone
        // $phone = $dataToCreate['phone'];
        // if (str_contains($phone, '+234') === false) {
        //     $phone = ltrim($phone, "0");
        //     $phone = '+234' . $phone;
        //     $dataToCreate['phone'] = $phone;
        // }
        // $phone = cleanupPhone($phone);
        // sendTokenViaSMS($phone, $phoneToken);

        // Save user to DB
        $saveUser = $this->modelInstance::create([
            //'phone' => $dataToCreate['phone'],
            'email' => $dataToCreate['email'],
            'password' => $dataToCreate['password'],
            'email_verified_at' => Carbon::now(),
            'phone_verified_at' => Carbon::now(),
        ]);

        // Create a User Profile
        $saveUserProfile = Profile::create([
            'user_id' => $saveUser->id,
            'firstname' => $dataToCreate['firstname'],
            'lastname' => $dataToCreate['lastname'],
            //'mobile1' => $dataToCreate['phone'],
        ]);

        // Save Email Token and Phone Token
        // $saveToken = Token::create([
        //     'user_id' => $saveUser->id,
        //     'email_token' => $emailToken,
        //     'phone_token' => $phoneToken,
        // ]);

        if ($saveUserProfile) {
            return $saveUserProfile;
        }

        return false;
    }

    public function loginUser($credentials): array
    {
        $response = [];

        // Validate Credentials
        if (!$token = JWTAuth::attempt($credentials)) {
            return $response[] = [
                'error' => true,
                'message' => 'Invalid Login credentials',
            ];
        }

        // Check If Email is Verified
        $emailVerified = $this->modelInstance::whereEmail($credentials['email'])->first();
        if ($emailVerified->email_verified_at == null) {
            return $response[] = [
                'error' => true,
                'message' => 'Please verify Email Address to continue',
            ];
        }

        // Check if user is Blacklisted
        $blackListed = Blacklist::whereUserId($emailVerified->id)->first();
        if ($blackListed) {
            return $response[] = [
                'error' => true,
                'message' => 'Your account has been temporary locked. Please Contact Us at ' . env('SUPPORT_EMAIL'),
            ];
        }

        $user = $this->modelInstance::with('profile')->whereEmail($credentials['email'])->first();

        return $response[] = [
            'error' => false,
            'token' => $token,
            'user' => $user,
            'message' => 'Logged In Successfully',
        ];

    }

    public function resendEmailVerification($email): bool
    {

        // Check User Exist
        $user = $this->modelInstance::whereEmail($email)->first();

        // Delete Old Token
        if ($user) {
            $token = Token::whereUserId($user->id)->get();
            $token->each->delete();

            // Set up new Token
            $emailToken = generateToken();

            // Save Email Token
            Token::create([
                'user_id' => $user->id,
                'email_token' => $emailToken,
            ]);

            // Send email
            $data = [
                'email' => $email,
                'name' => $user->firstname . ' ' . $user->lastname,
                'verification_code' => $emailToken,
            ];
            Mail::to($data['email'])->send(new VerifyEmail($data));

            return true;
        }

        return false;

    }

    public function sendEmailToken($email) {
        // Set up new Token
        $emailToken = generateToken();

        // Save Email Token
        ValidToken::create([
            'token' => $emailToken,
            'email' => $email,
        ]);

         // Send email
         $data = [
            'email' => $email,
            'verification_code' => $emailToken,
        ];
        Mail::to($data['email'])->send(new SendEmailToken($data));

        return true;
    }

    public function validateEmailToken($email, $token) {
        $validateToken =  ValidToken::where('email', $email)->where('token', $token)->first();

        if ($validateToken) {
            $validateToken->delete();
            return true;
        }

        return false;
    }

    public function resendPhoneVerification($phone): array
    {
        $response = [];

        // Check User Exist
        $user = $this->modelInstance::wherePhone($phone)->first();

        // Delete Old Token
        if ($user) {
            $token = Token::whereUserId($user->id)->get();
            $token->each->delete();

            // Set up new Token
            $phoneToken = generateToken();

            // Save Email Token
            Token::create([
                'user_id' => $user->id,
                'phone_token' => $phoneToken,
            ]);

            // Send sms
            $phone = cleanupPhone($phone);
            $sendSms = sendTokenViaSMS($phone, $phoneToken);

            return $response[] = [
                'error' => false,
                'message' => 'Initiated Request to send Sms',
                'data' => $sendSms,
            ];
        }

        return $response[] = [
            'error' => true,
            'message' => 'Phone Number not found',
        ];

    }

    public function verifyEmail($data): array
    {
        $response = [];
        $email = $data['email'];
        $token = $data['token'];

        // Get the user Info
        $user = $this->modelInstance::whereEmail($email)->first();
        if (!$user) {
            return $response[] = [
                'error' => true,
                'message' => 'Invalid Email Credentials',
            ];
        }

        // Check Token Exists
        $tokenExist = Token::whereEmailToken($token)->whereUserId($user->id)->first();
        if (!$tokenExist) {
            return $response[] = [
                'error' => true,
                'message' => 'Token Not Valid',
            ];
        }

        // Check if Email has been verifiefd before
        if ($user->email_verified_at != null) {
            return $response[] = [
                'error' => true,
                'message' => 'Email Verified Already',
            ];
        }

        $user->email_verified_at = Carbon::now();
        $user->save();

        return $response[] = [
            'error' => false,
            'message' => 'Email Address Verified successfully',
        ];

    }

    public function verifyPhone($data): array
    {
        $response = [];
        $phone = $data['phone'];
        $token = (int) $data['token'];

        if (str_contains($phone, '+234') === false) {
            $phone = ltrim($phone, "0");
            $phone = '+234' . $phone;
        }

        // Get the user Info
        $user = $this->modelInstance::wherePhone($phone)->first();
        if (!$user) {
            return $response[] = [
                'error' => true,
                'message' => 'Invalid Phone Credentials',
            ];
        }

        // Check Token Exists
        $tokenExist = Token::wherePhoneToken($token)->whereUserId($user->id)->first();
        if (!$tokenExist) {
            return $response[] = [
                'error' => true,
                'message' => 'Token Not Valid',
            ];
        }

        // Check if Token is Expired
        $currentDate = date('Y-m-d H:i:s');
        $differenceInSeconds = strtotime($currentDate) - strtotime($tokenExist->updated_at);

        if (intval($differenceInSeconds / 60) > 10) {
            return $response[] = [
                'error' => true,
                'message' => 'Token Expired',
            ];
        }

        // Check if Email has been verifiefd before
        if ($user->phone_verified_at != null) {
            return $response[] = [
                'error' => true,
                'message' => 'Phone Verified Already',
            ];
        }

        $user->phone_verified_at = Carbon::now();
        $user->save();

        return $response[] = [
            'error' => false,
            'message' => 'Phone Verified successfully',
        ];

    }

    public function forgotPassword($email): array
    {
        $response = [];
        $code = generatePasswordResetCode();
        $user = $this->modelInstance::whereEmail($email)->first();

        if ($user === null) {
            return $response[] = [
                'error' => true,
                'message' => 'Invalid Email address',
            ];
        }

        $profile = Profile::whereUserId($user->id)->first();

        // Send Password Reset Instructions
        $data = [
            'email' => $email,
            'name' => $profile->firstname . ' ' . $profile->lastname,
            'code' => $code,
        ];

        // Save the reset code to db
        $user->password_token = $code;
        $user->save();

        // TODO make token expire after 60 minutes

        Mail::to($email)->send(new ForgotPasswordEmail($data));
        return $response[] = [
            'error' => false,
            'message' => 'Password Instructions sent to Email',
        ];

    }

    public function recoverPassword($data): array
    {
        $response = [];
        $user = $this->modelInstance::whereEmail($data['email'])
            ->wherePasswordToken($data['code'])
            ->first();

        if (!$user) {
            return $response[] = [
                'error' => true,
                'message' => 'Invalid Account',
            ];
        }

        $user->password = Hash::make($data['password_again']);
        $user->save();

        return $response[] = [
            'error' => false,
            'message' => 'Password Recovered Successfully',
        ];

    }
}
