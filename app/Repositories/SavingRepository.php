<?php

namespace app\Repositories;

use App\Saving;
use App\SavingType;

class SavingRepository {

    public function get($saving_id)
    {
        $saving = Saving::whereId($saving_id)->firstOrFail();
        return $saving ? $saving  : null;
    }


    public function onceTime ($request)
    {
        $user = auth()->user();
        $wallet = $user->wallet;

        $amount = $request->amount;
        $title = $request->title;
        $frequency = $request->frequency;
        $interval = $request->interval;
        $pay_method = $request->pay_method;
        $lock = $request->lock;

        $minimum = config('app.minimum_trx_amount');

        if($wallet->balance < $amount){
            return [
                'error' => true,
                'message' => 'insuficient wallet fund!'
            ];
        }


        if ($pay_method == 'wallet') {

            if($wallet->balance < $amount){
                return [
                    'error' => true,
                    'message' => 'insuficient wallet fund!'
                ];
            }

            $wallet->update([
                'balance' => $wallet->balance - $amount,
            ]);
        }

        $savingType = SavingType::whereName($frequency)->firstOrFail();
            
        $saving = Saving::create([
            'user_id' => $user->id,
            'wallet_id' => $wallet->id,
            'type' => $savingType->id,
            'title' => $title,
            'amount' => $amount,
            'percentage' => config('app.saving_percentage'),
            'is_once' => $frequency == 'once' ? true : false,
            'is_lock' => $lock != null ? true : false, 
        ]);


        return [
            'error' => false,
            'success' => true,
            'message' => 'saving request successful',
            'data' => $saving
        ];

    }


    public function stopSaving($request)
    {
        $saving = $this->get($request->saving_id);

        if($saving)
        {

            $wallet = $user->wallet;

            $saving->update([
                'is_stopped' => true,
                'stops_at' => now(),
            ]);

            $wallet->update([
                'balance' => $wallet->balance + $saving->amount
            ]);

            
            return [
                'error' => false,
                'message' => $saving->title .' stopped successfully',
                'data' => $saving
            ];
        }

        return [
            'error' => true,
            'data' => null,
            'message' => 'Saving not found',
        ];
    }
}