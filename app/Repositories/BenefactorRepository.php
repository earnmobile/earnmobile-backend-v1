<?php
namespace App\Repositories;

use App\Benefactor;




class BenefactorRepository{

    /**
     * Add a new benefactior @param object $data
     */
    public function create($data)
    {
        $benefactor = Benefactor::create([
            'user_id' => $data['user']->id,
            'bank_id' => $data['bank_id'],
            'account_name' => $data['account_name'],
            'account_number' => $data['account_number'],
            'is_default' => $data['is_default'] ? true : false, 
        ]);

        return $benefactor;
    }


    /**
     * Get Benefactors for a users @param collection $user
     */
    public function getUserBenefactors($user)
    {
        return $user->benefactors;
    }


    /**
     * Get benfactor by @param int $benefactor_id
     */
    public function getBenefactor($benefactor_id)
    {
        return Benefactor::where('id', $benefactor_id)->first();
    }

}