<?php 
namespace App\Repositories;

use App\User;
use App\Wallet;
use App\Benefactor;
use Illuminate\Support\Facades\Http;
use App\Http\Requests\FundingRequest;
use App\Http\Requests\Transfer\BankTransferRequest;
use App\Http\Requests\Transfer\UserTransferRequest;

class WalletRepository {

    protected $modelInstance;
    
    public function __construct(Wallet $wallet)
    {
        $this->modelInstance = $wallet;
    }


    /**
     * Create Wallet for user @param $user
     */
    public function createWallet(User $user)
    {
        $response[] = null;

         $wallet = $user->wallet;

        if($wallet == false){
            $wallet = $this->modelInstance->create([
                'user_id' => $user->id,
                'balance' => 0,
                'pending_balance' => 0
            ]);
            
            return $wallet;
        }else{

            return [
                'error' => true,
                'message' => 'User already have a wallet'
            ];
        }
        

    }


    /**
     * Fund wallet with creadit card @param FundingRequest $request
     */
    public function fundWallet($request)
    {
        $code = generateCode();

        $user = auth()->user();
        $wallet = $user->wallet;

        $card = $request->card;
        $fullname = $request->fullname;
        $cvv = $request->cvv;
        $expire_date = explode('/', $request->expiry_date);
        $expire_month = $expire_date[0];
        $expire_year = $expire_date[1];
        $amount = $request->amount;
        $email = $user->email;


        $data = [
            'card_number' => $card,
            'cvv' => $cvv,
            'expiry_month' => $expire_month,
            'expiry_year' => $expire_year,
            'currency' => 'NGN',
            'amount' => $amount,
            'email' => $email,
            'fullname' => $fullname,
            'tx_ref' => $code,
            'redirect_url' => route('funding.validate')
        ];

        //Encrypt Data ready for 
        $key = config('services.flutter-wave.ek');
        $encrypt_data = openSSLEncryption(json_encode($data), $key);
        $initiate_payment = payWithCard($encrypt_data);


        if($initiate_payment['status'] != false && $initiate_payment['data'] != null){

            if(isset($initiate_payment['data']['meta']))
            {
                $mode = $initiate_payment['data']['meta']['mode'];
                $pin = $initiate_payment['data']['meta']['pin'];

                $authorization = [
                    'mode' => $mode,
                    'pin' => $pin,
                ];

                $data->authorization = $authorization;

                $encrypt_data = openSSLEncryption(json_encode($data), $key);
               

                $initiate_payment = payWithCard($encrypt_data);


                $status = $initiate_payment['data']['status'];
                $data = $initiate_payment['data'];


                if($status == true && $data['status'] == 'successful')
                {
                    $transaction_id = $data['data']['id'];

                    // Verify Transaction

                    $verify_transaction = verifyPayment($transaction_id);

                    if($verify_transaction['status'] == true){
                        if($verify_transaction['data']['reference'] == $code)
                        {

                            $transaction = $wallet->transactions()->create([
                                'trx_id' => $data['trx_ref'],
                                'type' => 'credit',
                                'amount' => $data['amount'],
                                'fee' => 0,
                                'reason' => 'fund wallet',
                                'status' => true,
                                'previous_balance' => $wallet->balance,
                                'new_balance' => $wallet->balance + $data['amount'],
                            ]);

                            $wallet->fundings()->create([
                                'ref' => $transaction->trx_id,
                                'transaction_id' => $transaction->id,
                                'amount' => $data['amount'],
                                'status' => true
                            ]);

                            $wallet->update([
                                'balance' => $wallet->balance + $data['amount']
                            ]);

                            if($wallet){
                                return true;
                            }
                            
                        }

                        return [
                            'error' => true,
                            'message' => 'transaction mismatch'
                        ];
                    }

                    
                }

                return [
                    'error' => true,
                    'message' => $initiate_payment['err']
                ];
                
            }
            
        }
        
        return [
            'error' => true,
            'message' => $initiate_payment['err']
        ];

    }


    /**
     * Bank transfer @param BankTransferRequest $request
     */

    public function bankTransfer(BankTransferRequest $request)
    {
        $user = auth()->user();
        $wallet = $user->wallet;

        $current_balace = $wallet->balance;

        $code = generateCode();

        $amount = $request->amount;
        $fee = trx_fee($amount);
        

        $bank = Bank::where('code', $request->bank)->first();

        if(!$bank){
            return [
                'error' => true,
                'message' => 'bank is not valid'
            ];
        }

        $benefactor = Benefactor::whereAccount_number($request->account_number)->first();

        if(!$benefactor){

             //Add benefactor
            $benefactor = Benefactor::create([
                'user_id' => $user->id,
                'bank_id' => $bank->id,
                'account_number' =>$request->account_number,
                'account_name' =>  $request->account_name,
            ]);
            
        }
        

        $debit_amount = $amount+$fee;
        $remark = $code . $request->remark;
        $check_balance = $current_balace > $debit_amount;

        if($check_balance) {

            $transfer_data = [
                'account_number' => $benefactor->acount_number,
                'bank' => $bank->code,
                'amount' => $amount,
                'currency' => 'NGN',
                'narration' => $remark,
                'beneficiary_name' => $benefactor->account_name,
                'reference' => $code,
                'callback_url' => $callback,
                'debit_currency' => 'NGN'
            ];

            $key = config('services.flutter-wave.sk');
            $endpoint = config('services.flutter-wave.url').'transfers';
            $response = Http::withHeaders(['Authorization' => 'Bearer '.$key])->post($endpoint, $data)->json();

            $reference = $response['data']['reference'];
            $tx_id = $response['data']['id'];
            $tx_ref = $response['data']['tx_ref'];


            if($response['status'] == 'success'){

                $validate_transaction = verifyPayment($tx_id);
                $validation_status = $validate_transaction['data']['status'];

                if($validation_status == 'successful' && $validate_transaction['err'] == null)
                {

                    $amount = $validate_transaction['data']['amount'];
                    $reference = $validate_transaction['data']['reference'];
                    $debit_amount = $amount+$fee;

                    if($reference == $tx_ref) {

                        $transaction = $wallet->transactions()->create([
                            'trx_id' => $data['trx_ref'],
                            'type' => 'debit',
                            'amount' => $amount,
                            'fee' => $fee,
                            'reason' => 'bank transfer',
                            'status' => true,
                            'previous_balance' => $wallet->balance,
                            'new_balance' => $wallet->balance - $debit_amount,
                        ]);

                        $transfer = $wallet->transfer()->create([
                            'transaction_id' => $transaction->id,
                            'ref' => $tx_ref,
                            'benefactor_id' => $benefactor->id,
                            'amount' => $amount,
                            'status' => true
                        ]);

                        $wallet->update([
                            'balance' => $wallet->balance - $debit_amount
                        ]);
                        
                        return $transaction;
                    }

                     return [
                        'error' => true,
                        'message' => 'Transaction Failed'
                    ];
                }

                 return [
                    'error' => true,
                    'message' => $validate_transaction['err']
                ];
            }

            return [
                'error' => true,
                'message' => 'Transaction Failed'
            ];
        }

        return [
            'error' => true,
            'message' => 'Insuficient Fund'
        ];
        
    }


    public function transferToUser(UserTransferRequest $request)
    {
        $user = auth()->user();
        $wallet = $user->wallet;
        $current_balace = $wallet->balance;

        $fee = 0;
        $amount = $request->amount;

        $check_balance = $current_balace > $amount;


        $checkCredential = is_numeric($request->credential);
        
        // Find Benefactor Using credendtials
        $benefactor = null;
        if($checkCredential == false && checkEmail($request->credential) == true){
            $benefactor = User::where('email', $request->credential)->first();
        }else{
            $benefactor =  User::where('phone', trim_phone($request->credential))->first();
        }
       

        $code = generateCode();
        

        if ($benefactor == true) {

            $benefactorWallet = $benefactor->wallet;

            //Check if current balance is above amount
            if ($check_balance) {

               
                $transaction = $wallet->transactions()->create([
                    'trx_id' => $code,
                    'type' => 'debit',
                    'amount' => $amount,
                    'fee' => $fee,
                    'reason' => 'transfer to user',
                    'status' => true,
                    'previous_balance' => $wallet->balance,
                    'new_balance' => $wallet->balance - $amount,
                ]);

                $wallet->update([
                    'balance' => $wallet->balance - $amount
                ]);


                $transfer = $wallet->transfer()->create([
                    'transaction_id' => $transaction->id,
                    'ref' => $code,
                    'benefactor_id' => $benefactor->id,
                    'amount' => $amount,
                    'remark' => $request->remark,
                    'status' => true,
                    'to_user' => true,
                ]);

                
                $benefactorCode = generateCode();
                
                $transaction2 = $benefactorWallet->transactions()->create([
                    'trx_id' => $benefactorCode,
                    'type' => 'credit',
                    'amount' => $amount,
                    'fee' => $fee,
                    'reason' => 'transfer to user',
                    'status' => true,
                    'previous_balance' => $benefactorWallet->balance,
                    'new_balance' => $benefactorWallet->balance - $amount,
                ]);

                $wallet->update([
                    'balance' => $benefactorWallet->balance + $amount
                ]);

                return $transaction;
            }

            return [
                'error' => true,
                'message' => 'Insuficient Fund'
            ];

        }else{
             return [
                'error' => true,
                'message' => 'Wrong credentials!'
            ];
        }

    }


    

}