<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavingType extends Model
{
    protected $guarded = ['id'];

    public $timestamps = false;

    /**
     * Get all of the savings for the SavingType
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function savings()
    {
        return $this->hasMany(Saving::class, 'type');
    }
}
