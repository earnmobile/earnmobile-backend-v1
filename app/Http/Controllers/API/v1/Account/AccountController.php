<?php

namespace App\Http\Controllers\API\v1\Account;

use App\Account;
use App\Helpers\ApiResponder;
use App\Helpers\Monnify;
use App\Http\Controllers\Controller;
use App\Repositories\AccountRepository;
use Illuminate\Http\JsonResponse;
use Throwable;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    private $monnifyToken;
    private $accountRepository;

    public function __construct(AccountRepository $authRepository)
    {
        $this->accountRepository = $authRepository;
        $this->monnifyToken = Monnify::authenticate()->responseBody->accessToken;
    }

    public function generateAccountNumber(): JsonResponse
    {
        try {
            $accountNumber = $this->accountRepository->generateAccountNumber($this->monnifyToken);

            if ($accountNumber->requestSuccessful) {
                return ApiResponder::successResponse($accountNumber->responseBody, $accountNumber->responseMessage);
            }

            return ApiResponder::errorResponse($accountNumber->responseMessage, 500);

        } catch (Throwable $err) {
            return ApiResponder::errorResponse($err->getMessage(), 500);
        }

    }

    public function webhook(Request $request) {
        try {

            Account::create([
                'user_id' => '1',
                'bank_name' => '2',
                'bank_code' => '3',
                'name' => '4',
                'number' => '4',
                'reference' => $request->all(),
            ]);

        } catch (Throwable $err) {
            return ApiResponder::errorResponse($err->getMessage(), 500);
        }
    }
}
