<?php

namespace App\Http\Controllers\API\v1\Saving;

use Illuminate\Http\Request;
use App\Helpers\ApiResponder;
use App\Http\Controllers\Controller;
use app\Repositories\SavingRepository;
use App\Http\Requests\Saving\StopRequest;
use App\Http\Requests\Saving\CreateRequest;

class SavingController extends Controller
{

    private $interface;

    public function __construct(SavingRepository $interface)
    {
        $this->interface = $interface;
    }


    public function oneTime(CreateRequest $request)
    {
        try {
            $createSaving = $this->interface->oneTime($request);

            if($createSaving['error'] == true) {
                return ApiResponder::errorResponse($createSaving['message'], 500);
            }

            return ApiResponder::successResponse($createSaving['data'], $createSaving['message'], 200);

        } catch (\Throwable $err) {
             return ApiResponder::errorResponse($err, 500);
        }
    }


    public function stop(StopRequest $request)
    {
        try {
            $stopSaving = $this->interface->stopSaving($request);

            if($stopSaving['error'] == false){
                return ApiResponder::successResponse($stopSaving['data'], $stopSaving['message'], 200);
            }

            return ApiResponder::errorResponse($stopSaving['message'], 500);

        } catch (\Throwable $err) {
             return ApiResponder::errorResponse($err, 500);
        }
    }
}
