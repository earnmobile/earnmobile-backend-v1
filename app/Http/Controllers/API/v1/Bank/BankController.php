<?php

namespace App\Http\Controllers\API\v1\Bank;

use Illuminate\Http\Request;
use App\Helpers\ApiResponder;
use App\Http\Controllers\Controller;
use App\Repositories\BankRepository;
use App\Http\Requests\Bank\ValidateAccount;
use Throwable;

class BankController extends Controller
{
    private $interface;

    public function __construct(BankRepository $interface)
    {
        $this->interface = $interface;
    }

    /**
     * Get a list of all banks
     */
    public function index()
    {
        try {
            $banks = $this->interface->all();

            return ApiResponder::successResponse($banks, 'All Banks');

        } catch (Throwable $err) {

            return ApiResponder::errorResponse($err->getMessage(), 500);

        }
    }


    /**
     * Get data of bank
     * @param $bank_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($bank_id)
    {
        try {
            $bank = $this->interface->get($bank_id);

            return ApiResponder::successResponse($bank, 'Data Fetched');

        } catch (Throwable $err) {

            return ApiResponder::errorResponse($err->getMessage(), 500);

        }
    }


    public function resolveAccount(ValidateAccount $request)
    {
        try {
            $validation = $this->interface->resolveAccount($request);

            if ($validation['status'] == false) {

                return ApiResponder::errorResponse($validation['message'], 500);
            }
            return ApiResponder::successResponse($validation['data'], 'account validated');

        } catch (Throwable $err) {

            return ApiResponder::errorResponse($err->getMessage(), 500);
        }
    }


}
