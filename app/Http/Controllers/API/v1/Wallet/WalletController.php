<?php

namespace App\Http\Controllers\API\v1\Wallet;

use Illuminate\Http\Request;
use App\Helpers\ApiResponder;
use App\Http\Controllers\Controller;
use App\Http\Requests\WalletRequest;
use App\Http\Requests\FundingRequest;
use App\Repositories\WalletRepository;
use App\Http\Requests\Transfer\BankTransferRequest;
use App\Http\Requests\Transfer\UserTransferRequest;

class WalletController extends Controller
{
    private $walletRepository;

    public function __construct(WalletRepository $walletRepository)
    {
        $this->walletRepository = $walletRepository;
    }

    /**
     * Create wallet for user @param(WalletRequest $request)
     */

    public function createWallet()
    {
        try {

            $user = auth()->user();


            $wallet = $this->walletRepository->createWallet($user);
               

            if($wallet['error']){

                return ApiResponder::errorResponse($wallet['message'], 500);

            }else{

                return ApiResponder::successResponse($wallet, 'Wallet created successfully', 200);

            }

           
        } catch (\Throwable $err) {

            return ApiResponder::errorResponse($err, 500);

        }
    }


    public function fundWallet(FundingRequest $request)
    {
        try {

            $wallet = $this->walletRepository->fundWallet($request);

            if($wallet) {
                return ApiResponder::successResponse($wallet, 'Wallet credited successfully', 200);
            }

            return ApiResponder::errorResponse('Wallet was not credited. Please try again', 500);

        } catch (\Throwable $err) {
           return ApiResponder::errorResponse($err, 500);
        }
    }


    public function bankTransfer(BankTransferRequest $request)
    {
        try {
           
            $transfer = $this->walletRepository->bankTransfer($request);

            if($transfer['error'] == false) {
                return ApiResponder::successResponse($transfer, 'Transaction was successful', 200);
            }else{
                
                return ApiResponder::errorResponse($transfer['error'], 500);
            }


        } catch (\Throwable $err) {

           return ApiResponder::errorResponse($err, 500);

        }
    }


    public function userTransfer(UserTransferRequest $request)
    {
        try {
           
            $transfer = $this->walletRepository->transferToUser($request);

            if($transfer['error'] == false) {

                return ApiResponder::successResponse($transfer, 'Transaction was successful', 200);
                
            }else{
                
                return ApiResponder::errorResponse($transfer['error'], 500);
            }


        } catch (\Throwable $err) {

           return ApiResponder::errorResponse($err, 500);

        }
    }

}
