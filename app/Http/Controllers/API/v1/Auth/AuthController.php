<?php

namespace App\Http\Controllers\API\v1\Auth;

use App\Helpers\ApiResponder;
use App\Http\Controllers\Controller;
use App\Http\Requests\ForgotPasswordRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RecoverPasswordRequest;
use App\Http\Requests\RegisterRequest;
use App\Repositories\AuthRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class AuthController extends Controller
{

    private $authRepository;

    public function __construct(AuthRepository $authRepository)
    {
        $this->authRepository = $authRepository;
    }

    public function register(RegisterRequest $request): JsonResponse
    {
        try {

            $registerUser = $this->authRepository->registerUser($request->all());

            if (!$registerUser) {
                return ApiResponder::errorResponse('Account was not created successfully. Please try again', 500);
            }

            return ApiResponder::successResponse($registerUser, 'Registered Successfully', 201);

        } catch (Throwable $err) {
            return ApiResponder::errorResponse($err->getMessage(), 500);
        }
    }

    public function login(LoginRequest $request): JsonResponse
    {
        try {

            $loginUser = $this->authRepository->loginUser($request->all());

            if ($loginUser['error'] == false) {

                return ApiResponder::successResponse([
                    'token' => $loginUser['token'],
                    'user' => $loginUser['user'],
                ], 'Authenticated');
            }

            return ApiResponder::errorResponse($loginUser['message'], 401);

        } catch (Throwable $err) {
            return ApiResponder::errorResponse($err->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function resendEmailVerification(Request $request): JsonResponse
    {
        try {
            $resendVerification = $this->authRepository->resendEmailVerification($request->email);

            if ($resendVerification) {
                return ApiResponder::successResponse(null, 'Email Re-sent Successfully');
            }

            return ApiResponder::errorResponse('Invalid Email Address', 500);

        } catch (Throwable $err) {
            return ApiResponder::errorResponse($err->getMessage(), 500);
        }
    }


    public function sendEmailToken(Request $request) {
        try {
            $sendEmailToken = $this->authRepository->sendEmailToken($request->email);

            if ($sendEmailToken) {
                return ApiResponder::successResponse(null, 'Email sent Successfully');
            }

        } catch (Throwable $err) {
            return ApiResponder::errorResponse($err->getMessage(), 500);
        }
    }

    public function validateEmailToken(Request $request) {
        try {
            $validateEmailToken = $this->authRepository->validateEmailToken($request->email, $request->token);

            if ($validateEmailToken) {
                return ApiResponder::successResponse(null, 'Email validated Successfully');
            }

            return ApiResponder::errorResponse('Invalid Token', 400);

        } catch (Throwable $err) {
            return ApiResponder::errorResponse($err->getMessage(), 500);
        }
    }

    public function resendPhoneVerification(Request $request)
    {
        try {
            $resendVerification = $this->authRepository->resendPhoneVerification($request->phone);

            if ($resendVerification['error'] == false) {
                return ApiResponder::successResponse([
                    'sms_api_data' => $resendVerification['data'],
                ], $resendVerification['message']);
            }

            return ApiResponder::errorResponse($resendVerification['message'], 500);

        } catch (Throwable $err) {
            return ApiResponder::errorResponse($err->getMessage(), 500);
        }
    }

    public function verifyEmail(Request $request): JsonResponse
    {
        try {

            $verifyEmail = $this->authRepository->verifyEmail($request->all());

            if ($verifyEmail['error'] == false) {
                return ApiResponder::successResponse(null, $verifyEmail['message']);
            }

            return ApiResponder::errorResponse($verifyEmail['message'], 500);

        } catch (Throwable $err) {
            return ApiResponder::errorResponse($err->getMessage(), 500);
        }
    }

    public function verifyPhone(Request $request): JsonResponse
    {
        try {

            $verifyPhone = $this->authRepository->verifyPhone($request->all());

            if ($verifyPhone['error'] == false) {
                return ApiResponder::successResponse(null, $verifyPhone['message']);
            }

            return ApiResponder::errorResponse($verifyPhone['message'], 500);

        } catch (Throwable $err) {
            return ApiResponder::errorResponse($err->getMessage(), 500);
        }
    }

    public function forgotPassword(ForgotPasswordRequest $request): JsonResponse
    {
        try {

            $forgotPassword = $this->authRepository->forgotPassword($request->email);
            if ($forgotPassword['error'] == false) {
                return ApiResponder::successResponse(null, $forgotPassword['message']);
            }

            return ApiResponder::errorResponse($forgotPassword['message'], 500);

        } catch (Throwable $err) {
            return ApiResponder::errorResponse($err->getMessage(), 500);
        }
    }

    public function recoverPassword(RecoverPasswordRequest $request): JsonResponse
    {
        try {

            $recoverPassword = $this->authRepository->recoverPassword($request->all());

            if ($recoverPassword['error'] == false) {
                return ApiResponder::successResponse(null, $recoverPassword['message']);
            }

            return ApiResponder::errorResponse($recoverPassword['message'], 500);

        } catch (Throwable $err) {
            return ApiResponder::errorResponse($err->getMessage(), 500);
        }
    }
}
