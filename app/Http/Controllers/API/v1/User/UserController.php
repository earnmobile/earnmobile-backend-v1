<?php

namespace App\Http\Controllers\API\v1\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\ApiResponder;
use App\Repositories\UserRepository;
use Throwable;

class UserController extends Controller
{

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getUser() {
        try {
            $user = $this->userRepository->getUser();

            if ($user) {
                return ApiResponder::successResponse($user);
            }

        } catch (Throwable $err) {
            return ApiResponder::errorResponse($err->getMessage(), 500);
        }
    }
}
