<?php

namespace App\Http\Controllers;

use ApiResponder;

class PublicController extends Controller
{
    public function healthz()
    {
        return ApiResponder::successResponse(null, 'Api is working');
    }
}
