<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|unique:users,email|email',
            'password' => 'required|min:6',
            'firstname' => 'required',
            'lastname' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'Email is required',
            'phone.required' => 'Phone No is required',
            'email.email' => 'Please provide a valid Email address',
            'password.required' => 'Password is required',
            'password.min' => 'Minimum Password length is six(6) characters',
            'lastname.required' => 'Last Name is required',
            'firstname.required' => 'First Name is required',
            'bvn.required' => 'BVN is required',
        ];
    }

}
