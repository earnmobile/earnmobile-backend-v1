<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FundingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * Please do note that expire_date will be seperated by /
     *
     * @return array
     */
    public function rules()
    {
        return [
            'card' => 'required|numeric',
            'cvv' => 'required',
            'expiry_date' => 'required',
            'amount' => 'required',
            'fullname' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'card.required' => 'Card is required',
            'cvv.required' => 'CVV is required',
            'expire_date.required' => 'Expire Date is required',
            'amount' => 'Amount is required',
            'fullname' => 'Fullname is required'
        ];
    }
}
