<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone', 'email', 'pin', 'password', 'email_verified_at', 'phone_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the token associated with the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function token(): HasOne
    {
        return $this->hasOne(Token::class);
    }

    /**
     * Get the account associated with the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function account(): HasOne
    {
        return $this->hasOne(Account::class);
    }

    /**
     * Get the profile associated with the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    /**
     * Get the wallet associated with the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function wallet(): HasOne
    {
        return $this->hasOne(Wallet::class);
    }

    /**
     * Get all of the savings for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function savings(): HasMany
    {
        return $this->hasMany(Saving::class);
    }

    /**
     * Get all of the beneficiaries for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function benefactors(): HasMany
    {
        return $this->hasMany(Benefactor::class);
    }

    /**
     * Get all of the default_benefactor for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function default_benefactor(): HasMany
    {
        return $this->benefactors()->where('is_default', true)->first();
    }

    /**
     * Get all of the saved cards for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cards(): HasMany
    {
        return $this->hasMany(SavedCard::class);
    }

    /**
     * Get all of the default saved card for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function default_card(): HasMany
    {
        return $this->cards()->where('is_default', true)->first();
    }

    /**
     * Get the verification associated with the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function verification(): HasOne
    {
        return $this->hasOne(Verification::class);
    }

    /**
     * Get the blacklist associated with the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function in_blacklist(): HasOne
    {
        return $this->hasOne(Blacklist::class);
    }
}
