<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditCardFunding extends Model
{
    protected $guarded = ['id'];

    /**
     * Get the wallet that owns the Funding
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wallet(): BelongsTo
    {
        return $this->belongsTo(Wallet::class);
    }

    /**
     * Get the transactions that owns the Funding
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transactions(): BelongsTo
    {
        return $this->belongsTo(Transaction::class);
    }

    
}
