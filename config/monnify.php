<?php

return [
    'base_url' => env('MONNIFY_BASE_URL'),
    'secret_key' => env('MONNIFY_SECRET_KEY'),
    'api_key' => env('MONNIFY_APIKEY'),
    'contract_code' => env('MONNIFY_CONTRACT_CODE')
];
