<?php

return [
    'base_url' => env('SMS_BASE_URL'),
    'sender' => env('SMS_SENDER'),
    'api_key' => env('SMS_API_KEY'),
];
