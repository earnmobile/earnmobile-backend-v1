<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSavingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('savings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('wallet_id');
            $table->integer('type')->unsigned();
            $table->string('title');
            $table->double('amount')->nullable()->default(0);
            $table->double('percentage');
            $table->boolean('is_once')->default(false);
            $table->boolean('is_lock')->default(false);
            $table->timestamp('stops_at')->nullable();
            $table->boolean('is_stopped')->nullable()->default(false);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('wallet_id')->references('id')->on('wallets');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('savings');
    }
}
