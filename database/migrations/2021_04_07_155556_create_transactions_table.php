<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('wallet_id');
            $table->string('trx_id')->nullable();
            $table->string('type')->nullable();
            $table->double('amount')->nullable()->default(0);
            $table->double('fee')->nullable()->default(0);
            $table->string('reason')->nullable();
            $table->boolean('status')->nullable()->default(false);
            $table->double('previous_balance')->nullable()->default(0);
            $table->double('new_balance')->nullable()->default(0);
            $table->foreign('wallet_id')->references('id')->on('wallets');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
