<?php

use App\SavingType;
use Illuminate\Database\Seeder;

class SavingTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        SavingType::truncate();
        
        $types = [
            [ 'name' => 'once' ],
            [ 'name' => 'daily' ],
            [ 'name' => 'weekly' ],
            [ 'name' => 'monthly' ],
        ];

        SavingType::insert($types);
    }
}
